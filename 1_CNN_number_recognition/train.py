import typing

from tensorflow import keras

if typing.TYPE_CHECKING:
    from keras.api._v2 import keras

# загружаем датасет из mnist
(X_train, y_train), (X_test, y_test) = keras.datasets.mnist.load_data()

# Подготовка данных для подачи их на вход сети (превращаем каждую картинку в двумерный массив)
batch_size, img_rows, img_cols = 64, 28, 28
X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 1)
X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 1)
input_shape = (img_rows, img_cols, 1)

# Нормализуем данные 0...1 вместо 0...255 (хотя можно было бы оставить целочисленными)
X_train = X_train.astype("float32")
X_test = X_test.astype("float32")
X_train /= 255
X_test /= 255

# Переведём правильные ответы в one-hot представление (вместо 5 будет [0 0 0 0 0 1 0 0 0 0])
Y_train = keras.utils.to_categorical(y_train, 10)
Y_test = keras.utils.to_categorical(y_test, 10)

# Создаём модель
model = keras.models.Sequential()
# Свёрточный слой с окном 5х5, 32 фильтра и на выходе мы получаем матрицу такого же размера, что и на входе
model.add(keras.layers.Convolution2D(32, 5, 5, padding='same', activation='relu', input_shape=input_shape))
# Слой субдискретизации (уменьшение размерности матрицы)
model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding="same"))

# Добавим точности модели путём добавления ещё 3х слоёв
model.add(keras.layers.Convolution2D(64, 5, 5, padding='same', activation='relu', input_shape=input_shape))
model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding="same"))

# Создаём полносвязные слои
model.add(keras.layers.Flatten())
# Слой из 1024 нейронов
model.add(keras.layers.Dense(1024, activation='relu'))
# Слой дропаута для устойчивости модели
model.add(keras.layers.Dropout(0.5))
# Выходной слой
model.add(keras.layers.Dense(10, activation='softmax'))


callbacks = [
    keras.callbacks.ModelCheckpoint("model.h5", save_best_only=True, monitor="accuracy")
]

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
model.fit(X_train, Y_train, batch_size=batch_size, epochs=10, validation_split=0.1, callbacks=callbacks)

model = keras.models.load_model("model.h5")

print("Проверка модели:")
model.evaluate(X_test, Y_test)
