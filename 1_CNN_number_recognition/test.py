import typing

import numpy as np
import tensorflow as tf
from tensorflow import keras

if typing.TYPE_CHECKING:
    from keras.api._v2 import keras

image_size = (28, 28)
img = keras.preprocessing.image.load_img("7.jpg", target_size=image_size, color_mode="grayscale")
img_array = keras.preprocessing.image.img_to_array(img)
img_array = tf.expand_dims(img_array, 0)
img_array /= 255

model = keras.models.load_model("model.h5")
predictions = model.predict(img_array)
score = predictions[0]

print(f"Похоже на цифру: {np.argmax(score)}")
print(f"Реальный вывод модели: {score}")
print(f"Отформатированный вывод модели: {np.around(score,4)}")
