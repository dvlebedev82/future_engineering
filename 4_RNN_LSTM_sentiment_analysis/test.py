import json
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
import typing

from tensorflow import keras

if typing.TYPE_CHECKING:
    from keras.api._v2 import keras
from keras.preprocessing.sequence import pad_sequences

with open('tokenizer.json') as f:
    data = json.load(f)
    tokenizer = keras.preprocessing.text.tokenizer_from_json(data)

reverse_word_map = dict(map(reversed, tokenizer.word_index.items()))
max_text_len = 10
model = keras.models.load_model("model.h5")


def sequence_to_text(list_of_indices):
    words = [reverse_word_map.get(letter) for letter in list_of_indices]
    return (words)


# t = "За помощь надо платить монетой".lower()
t = "Живи так чтобы увидеть, как ты становишься сильнее и живешь счастливой, полноценной жизнью.".lower()
data = tokenizer.texts_to_sequences([t])
data_pad = pad_sequences(data, maxlen=max_text_len)
print(sequence_to_text(data[0]))

res = model.predict(data_pad)
f_res = "Позитивненько" if np.argmax(res) == 0 else "Негативненько"
print(res, f_res, sep='\n')
