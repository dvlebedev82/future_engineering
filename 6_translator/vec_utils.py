import pickle

import tensorflow as tf
from keras.layers import TextVectorization


def save_vec(filename, vectorizer):
    pickle.dump({'config': vectorizer.get_config(), 'weights': vectorizer.get_weights(), 'vocab': vectorizer.get_vocabulary()},
                open(filename, "wb"))


def load_vec(filename):
    from_disk = pickle.load(open(filename, "rb"))
    vec = TextVectorization.from_config(from_disk['config'])
    vec.adapt(tf.data.Dataset.from_tensor_slices(["xyz"]))
    vec.set_weights(from_disk['weights'])
    vec.set_vocabulary(from_disk['vocab'])
    return vec
