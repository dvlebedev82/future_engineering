import os
import random
import re
import string

import numpy as np
import tensorflow as tf

from decoder import TransformerDecoder
from embedding import PositionalEmbedding
from encoder import TransformerEncoder

os.add_dll_directory("C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v11.2/bin")
from tensorflow import keras

import vec_utils

text_file = 'eng-rus.txt'
num_test_sentences = 30

with open(text_file, encoding="utf8") as f:
    lines = f.read().split("\n")[:-1]
    text_eng = []
    for line in lines:
        eng, _ = line.split("\t")
        text_eng.append(eng)

random.shuffle(text_eng)
test_sentences = text_eng[:num_test_sentences]

model = keras.models.load_model(
    "model.h5",
    custom_objects={'PositionalEmbedding': PositionalEmbedding, 'TransformerEncoder': TransformerEncoder, 'TransformerDecoder': TransformerDecoder}
)

strip_chars = string.punctuation
strip_chars = strip_chars.replace("[", "")
strip_chars = strip_chars.replace("]", "")


def custom_standardization(input_string):
    lowercase = tf.strings.lower(input_string, encoding="utf-8")
    return tf.strings.regex_replace(lowercase, "[%s]" % re.escape(strip_chars), "")


rus_vectorization = vec_utils.load_vec("rus_vec.h5")
eng_vectorization = vec_utils.load_vec("eng_vec.h5")

rus_vocab = rus_vectorization.get_vocabulary()
rus_index_lookup = dict(zip(range(len(rus_vocab)), rus_vocab))
max_decoded_sentence_length = 20


def decode_sequence(input_sentence):
    tokenized_input_sentence = eng_vectorization([input_sentence])
    decoded_sentence = "[start]"
    for i in range(max_decoded_sentence_length):
        tokenized_target_sentence = rus_vectorization([decoded_sentence])[:, :-1]
        predictions = model([tokenized_input_sentence, tokenized_target_sentence])

        sampled_token_index = np.argmax(predictions[0, i, :])
        sampled_token = rus_index_lookup[sampled_token_index]
        decoded_sentence += " " + sampled_token

        if sampled_token == "[end]":
            break
    return decoded_sentence


for sentence in test_sentences:
    translated = decode_sequence(sentence)
    print(f"{sentence} {translated}")
