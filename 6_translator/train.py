import os
import random
import re
import string

os.add_dll_directory("C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v11.2/bin")
import tensorflow as tf
from keras import layers
from keras.layers import TextVectorization
from tensorflow import keras

import vec_utils
from decoder import TransformerDecoder
from embedding import PositionalEmbedding
from encoder import TransformerEncoder

# Датасет скачан отсюда: http://www.manythings.org/anki/
text_file = 'eng-rus.txt'

with open(text_file, encoding="utf8") as f:
    lines = f.read().split("\n")[:-1]
    text_pairs = []
    for line in lines:
        eng, rus = line.split("\t")
        rus = "[start] " + rus + " [end]"
        text_pairs.append((eng, rus))

print("Пример пар (5 шт.):")
for _ in range(5):
    print(random.choice(text_pairs))

random.shuffle(text_pairs)
num_val_samples = int(0.15 * len(text_pairs))
num_train_samples = len(text_pairs) - 2 * num_val_samples
train_pairs = text_pairs[:num_train_samples]
val_pairs = text_pairs[num_train_samples: num_train_samples + num_val_samples]

print("\nРаспределение пар:")
print(f"{len(text_pairs)} всего пар")
print(f"{len(train_pairs)} тренировочных пар")
print(f"{len(val_pairs)} валидационных пар")

# Векторизация текста
strip_chars = string.punctuation
strip_chars = strip_chars.replace("[", "")
strip_chars = strip_chars.replace("]", "")

vocab_size = 15000
sequence_length = 20
batch_size = 64


def custom_standardization(input_string):
    lowercase = tf.strings.lower(input_string, encoding="utf-8")
    return tf.strings.regex_replace(lowercase, "[%s]" % re.escape(strip_chars), "")


eng_vectorization = TextVectorization(
    max_tokens=vocab_size,
    output_mode="int",
    output_sequence_length=sequence_length,
)
rus_vectorization = TextVectorization(
    max_tokens=vocab_size,
    output_mode="int",
    output_sequence_length=sequence_length + 1,
    standardize=custom_standardization,
)

train_eng_texts = [pair[0] for pair in train_pairs]
train_rus_texts = [pair[1] for pair in train_pairs]


eng_vectorization.adapt(train_eng_texts)
rus_vectorization.adapt(train_rus_texts)


# Сохраним на будущее векторайзеры
vec_utils.save_vec("eng_vec.h5", eng_vectorization)
vec_utils.save_vec("rus_vec.h5", rus_vectorization)


# Формирование датасетов
def format_dataset(eng, rus):
    eng = eng_vectorization(eng)
    rus = rus_vectorization(rus)
    return ({"encoder_inputs": eng, "decoder_inputs": rus[:, :-1], }, rus[:, 1:])


def make_dataset(pairs):
    eng_texts, rus_texts = zip(*pairs)
    eng_texts = list(eng_texts)
    rus_texts = list(rus_texts)
    dataset = tf.data.Dataset.from_tensor_slices((eng_texts, rus_texts))
    dataset = dataset.batch(batch_size)
    dataset = dataset.map(format_dataset)
    return dataset.shuffle(2048).prefetch(16).cache()


train_ds = make_dataset(train_pairs)
val_ds = make_dataset(val_pairs)

for inputs, targets in train_ds.take(1):
    print(f'inputs["encoder_inputs"].shape: {inputs["encoder_inputs"].shape}')
    print(f'inputs["decoder_inputs"].shape: {inputs["decoder_inputs"].shape}')
    print(f"targets.shape: {targets.shape}")

embed_dim = 256
latent_dim = 2048
num_heads = 8

encoder_inputs = keras.Input(shape=(None,), dtype="int64", name="encoder_inputs")
x = PositionalEmbedding(sequence_length, vocab_size, embed_dim)(encoder_inputs)
encoder_outputs = TransformerEncoder(embed_dim, latent_dim, num_heads)(x)
encoder = keras.Model(encoder_inputs, encoder_outputs)

decoder_inputs = keras.Input(shape=(None,), dtype="int64", name="decoder_inputs")
encoded_seq_inputs = keras.Input(shape=(None, embed_dim), name="decoder_state_inputs")
x = PositionalEmbedding(sequence_length, vocab_size, embed_dim)(decoder_inputs)
x = TransformerDecoder(embed_dim, latent_dim, num_heads)(x, encoded_seq_inputs)
x = layers.Dropout(0.5)(x)
decoder_outputs = layers.Dense(vocab_size, activation="softmax")(x)
decoder = keras.Model([decoder_inputs, encoded_seq_inputs], decoder_outputs)

decoder_outputs = decoder([decoder_inputs, encoder_outputs])
model = keras.Model([encoder_inputs, decoder_inputs], decoder_outputs, name="transformer")

epochs = 30  # Как минимум должно быть 30 эпох

callbacks = [
    keras.callbacks.ModelCheckpoint("model.h5", save_best_only=True),
]

model.summary()
model.compile("rmsprop", loss="sparse_categorical_crossentropy", metrics=["accuracy"])
model.fit(train_ds, epochs=epochs, validation_data=val_ds, callbacks=callbacks)
