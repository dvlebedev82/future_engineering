import typing

import tensorflow as tf
from tensorflow import keras

if typing.TYPE_CHECKING:
    from keras.api._v2 import keras


image_size = (180, 180)

img = keras.preprocessing.image.load_img("Bonya.jpg", target_size=image_size)
img_array = keras.preprocessing.image.img_to_array(img)
img_array = tf.expand_dims(img_array, 0)

model = keras.models.load_model("model.h5")
predictions = model.predict(img_array)
score = predictions[0]
print("Кот: %.2f , Собака: %.2f" % (100 * (1 - score), 100 * score))
