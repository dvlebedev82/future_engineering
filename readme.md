# Примеры для лекции на мероприятии "инженеры будущего"

### Рассмотрены модели:
<ol start="0">
<li>Нейронная сеть Цельсий-Фаренгейт</li>
<li>CNN Распознавание рукописной цифры 0...9</li>
<li>CNN Распознавание котов и собак</li>
<li>RNN Простейшая рекуррентная сеть предсказания следующего символа</li>
<li>RNN LSTM Определение тональности предложений</li>
<li>Автоэнкодер (восстановление зашумлённых изображений)</li>
<li>Простейший переводчик с английского на русский</li>
</ol>

### Скрипты проверены в следующей среде:
- python 3.8
- tensorflow 2.8.0
- pillow 9.0.1
- matplotlib 3.5.1

### Как установить поддержку GPU для Tensorflow
<ol>
<li>Узнать какие версии устанавливать CUDA Toolkit и cuDNN <a href="https://www.tensorflow.org/install/source#gpu">https://www.tensorflow.org/install/source#gpu</a></li>
<li>Установить Visual Studio <a href="https://visualstudio.microsoft.com/ru/vs/community/">https://visualstudio.microsoft.com/ru/vs/community/</a></li>
<li>Установить CUDA Toolkit <a href="https://developer.nvidia.com/cuda-toolkit-archive">https://developer.nvidia.com/cuda-toolkit-archive</a></li>
<li>Установить cuDNN <a href="https://developer.nvidia.com/cudnn">https://developer.nvidia.com/cudnn</a></li>
<li>Переписать файлы cuDNN в папку с CUDA Toolkit</li>
<li>Прописать пути до папок bin и libnvvp в переменные среды</li>
<li>
Если скрипт Python выводит ошибку "Could not load dynamic library 'cudart64_110.dll'", то написать код:
<br>
<code>
import os<br>
os.add_dll_directory("C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v11.2/bin")
</code>
<br>
Путь прописываете Ваш!
</li>
</ol>