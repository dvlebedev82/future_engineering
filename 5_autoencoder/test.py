import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
from keras.datasets import mnist
from tensorflow import keras
import matplotlib.pyplot as plt

(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Конвертируем набор в диапазон от 0 до 1
x_test = x_test / 255
x_test = np.reshape(x_test, (len(x_test), 28, 28, 1))

# Добавляем шум на изображения с помощью Гауссового распределения
noise_factor = 0.3
x_test_noisy = x_test + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_test.shape)
x_test_noisy = np.clip(x_test_noisy, 0., 1.)

model = keras.models.load_model("model.h5")

n = 10

imgs = x_test[:n]
decoded_imgs = model.predict(x_test_noisy[:n], batch_size=n)

plt.figure(figsize=(n, 3))
for i in range(n):
  ax = plt.subplot(3, n, i + 1)
  plt.imshow(imgs[i].squeeze(), cmap='gray')
  ax.get_xaxis().set_visible(False)
  ax.get_yaxis().set_visible(False)

  ax2 = plt.subplot(3, n, i + n + 1)
  plt.imshow(x_test_noisy[i].squeeze(), cmap='gray')
  ax2.get_xaxis().set_visible(False)
  ax2.get_yaxis().set_visible(False)

  ax3 = plt.subplot(3, n, i + n + n + 1)
  plt.imshow(decoded_imgs[i].squeeze(), cmap='gray')
  ax3.get_xaxis().set_visible(False)
  ax3.get_yaxis().set_visible(False)

plt.savefig('result.png')
